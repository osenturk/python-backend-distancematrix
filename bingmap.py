import requests
import numpy as np
import json
import urllib.parse
import pandas as pd
import sys
import time
import datetime as datetime

# r = requests.get('https://api.github.com/user', auth=('user', 'pass'))
#
# print(r.status_code)
# print(r.headers['content-type'])
# print(r.encoding)
# print(r.text)
# print(r.json())

now = datetime.datetime.now()
start_time = time.time()
output_file='bing_data/distance_matrix_'+str(now)[:16]+'.csv'

def getPoint(postCode):

    point = {}

    postCode = urllib.parse.quote_plus(postCode)
    url_get = 'http://dev.virtualearth.net/REST/v1/Locations/UK/'+postCode+'?&key=YOUR_BINGKEY'

    r = requests.get(url_get)

    temp = json.loads(r.text)

    # print(temp['resourceSets'])
    # print(temp['resourceSets'][0])
    # print(temp['resourceSets'][0]['resources'])
    # print(temp['resourceSets'][0]['resources'][0])
    # print(temp['resourceSets'][0]['resources'][0]['point'])
    # print(temp['resourceSets'][0]['resources'][0]['point']['coordinates'])
    # print(temp['resourceSets'][0]['resources'][0]['point']['coordinates'][0])

    point['latitude'] = temp['resourceSets'][0]['resources'][0]['point']['coordinates'][0]
    point['longitude'] =temp['resourceSets'][0]['resources'][0]['point']['coordinates'][1]


    return point

def getDistanceMatrixResponse(origins, destinations):

    datam={}
    datam['origins'] = origins
    datam['destinations'] = destinations
    datam['travelMode'] = 'driving'

    url_post = 'https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?key=' + bingkey

    r = requests.post(url_post,data=json.dumps(datam))

    temp = json.loads(r.text)

    results = temp['resourceSets'][0]['resources'][0]['results']

    return results


def convertPostcodes2Points(postCodes):

    origins=[]

    for postCode in postCodes:
        origins.append(getPoint(postCode))

    return origins

def parseResults(results):

    temp=[]
    for result in results:
        temp.append(result['travelDistance'])

    return temp

def getDistanceMatrix(origins, destinations,results):

    len_origin = len(origins)
    len_dest   = len(destinations)

    distance_matrix = np.matrix([np.zeros(len_dest) for y in range(len_origin)])

    fullfill_index=0
    for i in range(len_origin):

        for j in range(len_dest):
            distance_matrix[i,j]=results[fullfill_index]
            fullfill_index +=1


    return distance_matrix

def write_to_csv(distance_matrix,origins,destinations,output_file):

    temp = pd.DataFrame(distance_matrix,index=origins, columns=destinations)
    print(temp.head())
    temp.to_csv(path_or_buf=output_file)



bingkey='YOUR_BINGKEY'

input_file=sys.argv[1]
df=pd.read_csv(input_file, header=-1)

origins=df.iloc[:,0].tolist()

originPoints = convertPostcodes2Points(origins)

response = getDistanceMatrixResponse(originPoints,originPoints)

results = parseResults(response)

distance_matrix = getDistanceMatrix(originPoints,originPoints,results)

print(distance_matrix)

write_to_csv(distance_matrix,origins,origins,output_file)

minutes=(time.time() - start_time )/60

print("completed in {0:.2f} minutes ---".format(minutes))
