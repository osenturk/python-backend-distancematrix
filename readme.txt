Distance Matrix Tools

A set of Python Tools to help you calculate distance matrix between multiple locations

googlemap:
reads the 'input_parameters.txt' as an input and generate distance matrix. Google key is a must.

selen_openmap:
imitates user by browsing on openstreetmap.org and collect the distances for each requests

For general queries and support, please contact Ozan Senturk
(ozan.senturk@gmail.com)

Yours truly,

Ozan Senturk

