import googlemaps
import datetime as datetime
import sys
import pandas as pd
import time
import numpy as np

#[{'distance': {'text': '5.4 mi', 'value': 8630}, 'duration': {'text': '31 mins', 'value': 1876}, 'duration_in_traffic': {'text': '22 mins', 'value': 1303}, 'status': 'OK'},
# {'distance': {'text': '6.6 mi', 'value': 10692}, 'duration': {'text': '33 mins', 'value': 1971}, 'duration_in_traffic': {'text': '25 mins', 'value': 1512}, 'status': 'OK'},
# {'distance': {'text': '5.8 mi', 'value': 9341}, 'duration': {'text': '33 mins', 'value': 1998}, 'duration_in_traffic': {'text': '24 mins', 'value': 1419}, 'status': 'OK'},
# {'distance': {'text': '5.1 mi', 'value': 8198}, 'duration': {'text': '29 mins', 'value': 1761}, 'duration_in_traffic': {'text': '19 mins', 'value': 1166}, 'status': 'OK'}]

#<class 'dict'>: {'destination_addresses': ['London N1, UK', 'London N16 6YA, UK', 'London E1 6GJ, UK', 'London EC1Y 1BE, UK'], 'origin_addresses': ['Springfield Rd, London NW8 0QU, UK'], 'rows': [{'elements': [{'distance': {'text': '5.4 mi', 'value': 8630}, 'duration': {'text': '31 mins', 'value': 1876}, 'duration_in_traffic': {'text': '22 mins', 'value': 1317}, 'status': 'OK'}, {'distance': {'text': '6.6 mi', 'value': 10692}, 'duration': {'text': '33 mins', 'value': 1971}, 'duration_in_traffic': {'text': '25 mins', 'value': 1489}, 'status': 'OK'}, {'distance': {'text': '5.7 mi', 'value': 9146}, 'duration': {'text': '34 mins', 'value': 2037}, 'duration_in_traffic': {'text': '24 mins', 'value': 1430}, 'status': 'OK'}, {'distance': {'text': '5.1 mi', 'value': 8198}, 'duration': {'text': '29 mins', 'value': 1761}, 'duration_in_traffic': {'text': '20 mins', 'value': 1227}, 'status': 'OK'}]}], 'status': 'OK'}

print("distance matrix creation starts..")
now = datetime.datetime.now()

start_time = time.time()

output_file='data/google_distance_matrix_'+str(now)[:16]+'.csv'

def getDistances(result):

    distances=[]
    # destinations=result['destination_addresses']
    # origins = result['origin_addresses']

    for one_element in result['rows']:

        if len(one_element['elements']) != 0:
            one_dict=one_element['elements'][0]['distance']
            temp=one_dict['text']

            #currently only km
            index_dash = temp.find('km')
            if (index_dash != -1):
                distance = temp[:index_dash]
                distance = format(float(distance), '.3f')
            else:
                index_zero = temp.find('m')
                if (index_zero != -1):
                    distance = float(temp[:index_zero])
                    distance = format(distance / 1000, '.3f')

            distances.append(distance)

    return distances

def write_to_csv(distance_matrix,origins,destinations,output_file):

    temp = pd.DataFrame(distance_matrix,index=origins, columns=destinations)
    print(temp.head())
    temp.to_csv(path_or_buf=output_file)

#setup input parametershttp://sentr.co.uk
input_file=sys.argv[1]

df=pd.read_csv(input_file,delimiter='=',comment='#',header=-1)

defe=pd.read_csv('data/PostcodesList.csv', header=-1)
origins=defe.iloc[:,0].tolist()
destinations=origins

#origins=df.iloc[0,1].split(',')
#destinations=df.iloc[1,1].split(',')

mode=df.iloc[2,1]
if (pd.isnull(mode) == True):
    mode=None

unit=df.iloc[3,1]
if (pd.isnull(unit) == True):
    unit=None

departure_time = None
if (pd.isnull(df.iloc[4,1]) != True):
    departure_time=datetime.strptime(df.iloc[4,1], '%Y-%m-%d %H:%M:%S')

arrival_time = None
if (pd.isnull(df.iloc[5,1]) != True):
    arrival_time=datetime.strptime(df.iloc[5,1], '%Y-%m-%d %H:%M:%S')

if (pd.isnull(departure_time) == True) and (pd.isnull(arrival_time) == True):
    departure_time=now
    arrival_time = None
elif (pd.isnull(departure_time) != True):
    arrival_time = None

transit_mode=df.iloc[6,1]
if (pd.isnull(transit_mode) == True):
    transit_mode=None

traffic_model=df.iloc[7,1]
if (pd.isnull(traffic_model) == True):
    traffic_model=None

googlekey=df.iloc[8,1]
if (pd.isnull(traffic_model) == True):
    traffic_model=None

gmaps = googlemaps.Client(key=googlekey)

len_orig = len(origins)
len_dest = len(destinations)
distance_matrix = np.matrix([np.zeros(len_dest) for y in range(len_orig)])

# Request directions via public transit

for j in range(len_dest):
    directions_result = gmaps.distance_matrix(origins,
                                     destinations[j],
                                     mode=mode,
                                        units=unit,
                                          departure_time=departure_time, arrival_time=arrival_time, transit_mode=transit_mode,
                                          transit_routing_preference=None, traffic_model=traffic_model, region=None
                                          )
    distances=[]

    distances = getDistances(directions_result)
    print(distances)

    if  len(distances) != 0:
        for i in range(len_orig):
            distance_matrix[i,j]=distances[i]

    print(distance_matrix)

print(distance_matrix)


write_to_csv(distance_matrix,origins,destinations,output_file)

minutes=(time.time() - start_time )/60

print("completed in {0:.2f} minutes ---".format(minutes))