import time
import sys
import pandas as pd
import datetime as datetime
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import numpy as np

#######################
from geopy.geocoders import Nominatim

print("distance matrix creation starts..")
now = datetime.datetime.now()
start_time = time.time()


output_file='data/opens_distance_matrix_'+str(now)[:16]+'.csv'
#setup input parametershttp://sentr.co.uk
input_file=sys.argv[1]

##df=pd.read_csv(input_file,delimiter='=',comment='#',header=-1)
df=pd.read_csv(input_file, header=-1)

origins=df.iloc[:,0].tolist()


def makeup_url(lon,lat):

    openmap_url = u'https://www.openstreetmap.org/directions?engine=graphhopper_car&route={0:f}%2C{1:f}%3B{2:f}%2C{3:f}'.format(lon[0],lon[1],lat[0],lat[1])

    return openmap_url

def write_to_csv(distance_matrix,origins,output_file):
    temp = pd.DataFrame(distance_matrix,index=origins, columns=origins)

    print(temp.head())
    print('total number of requests is {0:d}'.format(how_many_request))

    temp.to_csv(path_or_buf=output_file)




geolocator = Nominatim(user_agent="sentrworks")

distance_dict=dict()
index = 0

length = len(origins)
total_requests = length * (length+1) * 0.5

distance_matrix = np.matrix([np.zeros(length) for y in range(length)])

how_many_request=0
browser = webdriver.Chrome('/Users/ozansenturk/PycharmProjects/tweep/chromedriver')

for i in range(length):

    location_orig = geolocator.geocode(origins[i])

    orig_point = (location_orig.latitude,location_orig.longitude )
    print('index {0:d} origin address: {1:s}'.format(i,location_orig.address))

    distance_per_row = []
    for j in range(length):

        if (j < i):

            distance_matrix[i,j]=distance_matrix[j,i]

        elif (j != i):

            try:
                time.sleep(1)
                # if(how_many_request==3):
                #     raise Exception('I know Python!')
                location_dest = geolocator.geocode(origins[j])
            except:
                time.sleep(10)
                try:
                    # if (how_many_request == 3):
                    #     raise Exception('I know Python!')
                    location_dest = geolocator.geocode(origins[j])
                except:
                    write_to_csv(distance_matrix, origins)
                    print('too many timeouts')
                    raise Exception('Too many timeouts..')

            #print('origin address:'+location_dest.address)
            dest_point=(location_dest.latitude, location_dest.longitude)

            ############# Selenium ################
            # browser = webdriver.Chrome()
            how_many_request +=1
            print('The {0:.2f}% has been completed'.format(how_many_request*100/total_requests))

            openstreetmap_url = makeup_url(orig_point,dest_point)
            browser.get(openstreetmap_url)

            #print("opening openrouteservice.org ... \n")
            time.sleep(2)
            try:
                text_summary = browser.find_element_by_id("routing_summary").text
                index_dash = text_summary.find('km')
                if (index_dash != -1):
                    distance = text_summary[10:index_dash]
                    distance_matrix[i,j]=float(distance)
                else:
                    index_zero = text_summary.find('m')
                    if (index_zero != -1):
                        distance = float(text_summary[10:index_zero])
                        distance = format(distance/1000, '.3f')
                        distance_matrix[i,j]=distance

                #print(text_summary)
            except NoSuchElementException:
                print('origin:'+origins[i]+'to destination: ' +origins[j]+' cannot be found')

        ########################################

        distance_dict[origins[i]]=distance_matrix[i]

    print(distance_matrix)

# Specify a writer
##writer = pd.ExcelWriter('distance_matrix_v3.xlsx', engine='xlsxwriter')



write_to_csv(distance_matrix,origins)
browser.close()

minutes = (time.time() - start_time) / 60
print("completed in {0:.2f} minutes ---".format(minutes))